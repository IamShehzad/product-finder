<?php

use App\Http\Controllers\Api\ShopController;
use App\Models\Product;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/images', function () {
	$product = Product::pluck('thumbnail');
    return view('images',compact('product'));
});
//access location
Route::get('/location',[ShopController::class,'getLocation']);
