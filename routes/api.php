<?php

use App\Http\Controllers\Api\AccountController;
use App\Http\Controllers\Api\BrandController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\ShopController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login',[UserController::class,'login']);
Route::post('products/search',[ProductController::class,'searching']);
Route::post('viewproduct',[ProductController::class,'view']);
Route::post('register',[UserController::class,'register']);
Route::post('countries',[UserController::class,'fetchCountry']);
Route::post('emailvalidation',[UserController::class,'checkEmail']);
Route::post('shoplocation',[ShopController::class,'getLocation']);

Route::post('products/finding',[ProductController::class,'finding']);
//we protect our routes
Route::group(['middleware'=>['auth:api']],function(){
	Route::apiResource('roles',RoleController::class);
	Route::apiResource('users',UserController::class);
	Route::apiResource('brands',BrandController::class);
	Route::apiResource('categories',CategoryController::class);
	Route::apiResource('products',ProductController::class);
	Route::apiResource('shops',ShopController::class);
	Route::apiResource('account',AccountController::class);
	Route::post('products/categories',[ProductController::class,'fetchCategory']);
	Route::post('products/keywords',[ProductController::class,'fetchKeywords']);
	
	Route::post('products/brands',[ProductController::class,'fetchBrand']);
	Route::post('products/dashboard',[ProductController::class,'fetchDashboard']);
	Route::post('products/delete',[ProductController::class,'deleteAll']);
	Route::post('users/delete',[UserController::class,'deleteAll']);
	Route::post('categories/delete',[CategoryController::class,'deleteAll']);
	//Route::post('/password',[UserController::class,'Matching']); //error
	Route::get('/verify', [UserController::class,'verify']);
	Route::post('brands/delete',[BrandController::class,'deleteAll']);
});
/*
{'name':this.editProduct.name,'description':this.editProduct.description,'price':this.editProduct.price,'keywords':this.editProduct.keywords,'category':this.editProduct.category,'brand':this.editProduct.brand,'image':this.editProduct.image}*/