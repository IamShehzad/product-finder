<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        //

        $per_page = $req->per_page;
        $user = Auth::user();
        $brands = Brand::where('user_id',$user->id)->where('id','<>',0)->paginate($per_page);
        return response()->json(['brands'=> $brands],200);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $user = Auth::user();
        $brand = Brand::where([
            ['name', '=', $request->name],
            ['user_id', '=', $user->id]])->first();
        
        if(!$brand) //user not exist
        {
            $brand = Brand::create([
                'name' => $request->name,
                'user_id' => $user->id
            ]);
            return response()->json(['brand'=>$brand],200);
        }
        //not exist
        return response()->json(['status'=>'The Brand is already Exists'],403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand = Brand::find($id);
        $brand->name = $request->name;
        $brand->save();
        return response()->json(['brand'=>$brand],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::find($id)->delete();
        return response()->json(['brand' => $brand],200);
    }
    public function deleteAll(Request $req)
    {
        Brand::whereIn('id',$req->brands)->delete();
        return response()->json(['message'=>'All Record Deleted Successfully'],200);
    }
}
