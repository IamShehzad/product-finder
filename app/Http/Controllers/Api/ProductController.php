<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Auth;
use Illuminate\Http\Request;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        //
        $per_page = $req->per_page;
        $user = Auth::user();
        $shop_id = $user->shopkeeper->shop->id;
        $products = new ProductCollection(Product::where('shop_id',$shop_id)->paginate($per_page));
       
       return response()->json(['products'=> $products],200);
      
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        //
         $user = Auth::user();
         $shop_id = $user->shopkeeper->shop->id;
       /* if($req->file('image'))
            return 'image exists';
        else
            return 'no image attached';*/
        $name = $req->name;
        $description = $req->description;
        $price = $req->price;

        $keyword = explode(',', $req->keywords);
        $keywords = json_encode($keyword,JSON_FORCE_OBJECT);
        $image = $req->image;
        $category = Category::where('name',$req->category)->first();
        $brand = Brand::where('name',$req->brand)->first();

        if($req->hasFile('image'))
        {
            
            $ext = $req->image->extension();
            $filename = sprintf("product_%s.".$ext,rand(1,100000));
            $filename = $req->file('image')->storeAs('product',$filename,'public');
            $product = Product::create([
                'name' => $name,
                'description' => $description,
                'price' => $price,
                'thumbnail' => $filename,
                'keywords' => $keywords,
                'category_id' => $category->id,
                'brand_id' => $brand->id,
                'shop_id' => $shop_id,

            ]);
            return response()->json(['product'=>$product],200);
        }
        else{
            $product = Product::create([
                'name' => $name,
                'description' => $description,
                'price' => $price,
                'thumbnail' => 'product/dummy.png',
                'keywords' => $keywords,
                'category_id' => $category->id,
                'brand_id' => $brand->id,
                'shop_id' => $shop_id

            ]);
            return response()->json(['product'=>$product],200);
        }
        
    }

 
    public function show($id)
    {
        //
        return 'Hi this is albert api';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $user = Auth::user();
         $shop_id = $user->shopkeeper->shop->id;
       // return response()->json(['data'=>$json,'orginal'=>$orginal],200);
     // return response()->json(['category'=>$req->all()],200);
        if($req->hasFile('image'))
        {
            $ext = $req->image->extension();
            return response()->json(['ext'=>$ext],200);
        }
        else{
            $category = Category::where('name',$req->category)->first();
            $brand = Brand::where('name',$req->brand)->first();
            $product = Product::find($id);
            $keywords = json_encode($req->keywords,JSON_FORCE_OBJECT);
            $product->name = $req->name;
            $product->description=$req->description;
            $product->price = $req->price;
            $product->keywords=$keywords;
            $product->category_id = $category->id;
            $product->brand_id = $brand->id;
            $product->shop_id = $shop_id;
            $product->save();
            $product->category_id = $req->category;
            $product->brand_id = $req->brand;
            return response()->json(['product'=>$product],200);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $product = Product::find($id)->delete();
        return response()->json(['status'=>'Product Deleted Successfully']);
    }
    public function fetchCategory()
    {
        $user = Auth::user();
        $categories = Category::where('user_id',$user->id)->orWhere('user_id',0)->pluck('name');
        if(!$categories)
            return response()->json(['status'=>'No Category are Found']);
        return response()->json(['categories'=>$categories]);
    }
    public function fetchBrand()
    {
        $user = Auth::user();
        $brands = Brand::where('user_id',$user->id)->orWhere('user_id',0)->pluck('name');
        if(!$brands)
            return response()->json(['status'=>'No Brand are Found']);
        return response()->json(['brands'=>$brands]);
    }
    public function fetchKeywords(Request $req)
    {

        $product = Product::where('id',$req->id)->first();
        $keywords = json_decode($product->keywords,JSON_FORCE_OBJECT);
        return response()->json(['keywords'=>$keywords],200);
    }
    public function searching(Request $req)
    {
        $search = $req->search;
        $products = Product::where('name', 'like', $search.'%')->pluck('keywords');
      //  return json_decode($products,JSON_FORCE_OBJECT);
        $size = count($products);
      //  return $size;
        if($size>0)
        {
            $keywords = [];
            $imp=[];
            $key = [];
            for($i=0;$i<$size;$i++)
            {
                $keywords[]=json_decode($products[$i],JSON_FORCE_OBJECT); 
            }
            $keywords = call_user_func_array('array_merge', $keywords); //merge array of array
            return response()->json(['keywords'=>$keywords],200);
        }
        // else
        //     return response()->json(['keywords'=>['Not Found']]);
       
        
    }
    public function deleteAll(Request $req)
    {
        Product::whereIn('id',$req->products)->delete();
        return response()->json(['message'=>'All Record Deleted Successfully'],200);
    }
    public function finding(Request $req)
    {
        $products = Product::where('name', 'like', $req->data.'%')->paginate(10);
        return response()->json(['products'=>$products],200);
    }
    public function fetchDashboard(Request $req)
    {
        $user = Auth::user();
        $shop_id = $user->shopkeeper->shop->id;
        $products = Product::where('shop_id',$shop_id)->count();
        $brands = Brand::where('user_id',$user->id)->count();
        $categories = Category::where('user_id',$user->id)->count();
        $price = Product::pluck('price');
        return response()->json(['products'=>$products,'brands'=>$brands,'categories'=>$categories,'price'=>$price],200);

        //-1 denoted 1 is default i.e [no brands,no category]
    }
    public function view(Request $req)
    {
        $p = Product::find($req->id);
        return response()->json(['product'=>$p],200);
    }
}

// 'name':this.product.name,'description':this.product.description,'price':this.product.price,'image':this.product.image,'keywords':this.product.keywords