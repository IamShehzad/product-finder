<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Auth;
use Illuminate\Http\Request;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        //
        $per_page = $req->per_page;
        $user = Auth::user();
        $categories = Category::where('user_id',$user->id)->where('id','<>',0)->paginate($per_page);
        // $categories = Arr::except($categories->data, ['price']);
        return response()->json(['categories'=> $categories],200);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        //
        $user = Auth::user();
        $category = Category::where([
            ['name', '=', $req->name],
            ['user_id', '=', $user->id]
        ])->first();
        if(!$category)
        {
            $category = Category::create([
                'name'=>$req->name,
                'description'=>$req->description,
                'user_id' => $user->id
                ]);
            unset($category->user_id);
            return response()->json(['category'=>$category],200);
        }
        return response()->json(['status'=>'The Category is already Exists'],403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        //
        $user = Auth::user();
        $category = Category::find($id);
        if($category) //category finded
        {
            if($category->name!=$req->name)
            {
                $category->name = $req->name;
                $category->description = $req->description;
                $category->user_id = $user->id;
                $category->save();
                return response()->json(['category'=>$category],200);
            }
            else if($category->description!=$req->description){
                $category->name = $req->name;
                $category->description = $req->description;
                $category->user_id = $user->id;
                $category->save();
                return response()->json(['category'=>$category],200);
            }
            else
                 return response()->json(['status'=>'Category Name or Description are Same,Try New!'],403);
        }
        else
           return response()->json(['status'=>'No Category found to be Update'],403);
           
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $category = Category::find($id)->delete();
        Product::whereCategoryId($id)->update(['category_id' => 0]);
        return response()->json(['category'=>$category],200);
    }
 
    public function deleteAll(Request $req)
    {
        Category::whereIn('id',$req->categories)->delete();
        return response()->json(['message'=>'All Record Deleted Successfully'],200);
    }
}
