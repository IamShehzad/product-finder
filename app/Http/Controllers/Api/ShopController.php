<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Shop;
use App\Models\Shopkeeper;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = Auth::user();
        $shopkeeper = Shopkeeper::where('user_id',$user->id)->first();
        $shop = Shop::where('shopkeeper_id',$shopkeeper->id)->first();
        return response()->json(['shop'=> $shop],200);
    }

    public function getLocation(Request $req)
    {
        $lat = $req->lat;
        $lng = $req->lng;
        // return $req->lat.' '.$req->lng;
        // $lat = 34.068964;
        // $lng = 73.150413;
        $input = $req->input;
        if(empty($input))
            $input = 'fair';
        
        // return $input;
        // records start
        $shops = Shop::select(DB::raw("
            *, ( 6371 * acos( cos( radians('$lat') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$lng') ) + sin( radians('$lat') ) * sin( radians( latitude)))) AS distance
            "))
            ->having('distance','<',2)
            ->orderBy('distance')
            ->get();
            // return $shops[0]->id;
        if($shops) //found
        {
             //records end
             //ids plucking starts [id of shops]
                 $ids = [];
                 foreach ($shops as $k =>$shop){
                    $ids[$k] = $shop->id;
                 }
                 $len = count($ids); //length of shops

                // return $ids;
             $products = Product::select('*')->where('name','like',$input.'%')->whereIn('shop_id',$ids)->get();
             //return 'shop:'.count($shops).' product:'.count($products).' len:'.$len;

             $length = count($products);

             if(count($products) >= 1)  
             {

                for($i=0;$i<$length;$i++) //loop 2
                {
                    
                    $products[$i]->distance = $this->shopDistance($shops,$products[$i]->id,'distance');
                   //$products[$i]->shop_id = $products[$i]->shop->name;
                    $products[$i]->shop_id = $this->shopDistance($shops,$products[$i]->id,'name');
                   
                }
             }
             
            
            if(sizeof($products) != 0)//found and not empty
            {
                return response()->json(['shops'=>$shops,'products'=>$products],200);
            }
            else
                return response()->json(['shops'=>$shops,'message'=>'No Products found in your nearist shops'],403);
            //return $products;

            
        }
        
        

        //ids plucking starts
    }
    public function shopDistance($shops,$product_id,$mode)
    {
        $len = count($shops);
        $product = Product::where('id',$product_id)->first();
        $shop_name = $product->shop->name;
        if($mode == 'distance')
        {
            for($i=0;$i<$len;$i++)
            {
                if($shop_name==$shops[$i]->name)
                    $distance = $shops[$i]->distance;
            }
            return round($distance,2);
        }
        else if($mode == 'name')
        {
            return $shop_name;
        }
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
