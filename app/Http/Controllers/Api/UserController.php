<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Shop;
use App\Models\Shopkeeper;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
class UserController extends Controller
{
     /**
     * Authenticate and login the user if it exist or not.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $req)
    {
        
        $credentials = $req->only('email','password');
        if(Auth::attempt($credentials))
        {
            //user exits 
            $token = Str::random(80);
            Auth::user()->api_token = $token;
            Auth::user()->save();
           $shopkeeper = Auth::user()->isShopkeeper();
            //we check the user is admin or not Auth::user()->isAdmin return
            //true if it is administrator otherwise false
            return response()->json(['token'=> $token,'isShopkeeper'=>$shopkeeper],200);
        }
        return response()->json(['message'=>'Invalid Email or Password'],403);
    }
    public function register(Request $req)
    {
        //register:{ fname: '',lname:'',dob:'',phone:'',street:'',city:'',country:'',postal_code:'',shop_name:'',location:'',latitude:'',longitude:'',address:'',email:'',pwd:'',cpwd:''},
        
        $token = Str::random(80);
        $user = User::create([
            'name' => 'Shokeeper',
            'email' => $req->email,
            'password'=> bcrypt($req->password),
            'role_id' => 3,
            'api_token' => $token
        ]);
        if($user)
        {
            $country = Country::where('country_name',$req->country)->first();
            $shopkeeper = Shopkeeper::create([
                'user_id'=> $user->id,
                'f_name' =>$req->fname,
                'l_name'=> $req->lname,
                'dob'=> $req->dob,
                'phone' => (int)$req->phone,
                'street'=> $req->street,
                'city'=> $req->city,
                'country_id'=> $country->id,
                'postal_code' => (int)$req->postal_code
            ]);
            //register:{ fname: '',lname:'',dob:'',phone:'',street:'',city:'',country:'',postal_code:'',shop_name:'',location:'',latitude:'',longitude:'',address:'',email:'',pwd:'',cpwd:''},
            if($shopkeeper)
            {
                $shop = Shop::create([
                    'shopkeeper_id'=>$shopkeeper->id,
                    'name'=>$req->shop_name,
                    'location'=>$req->location,
                    'address'=>$req->address,
                    'latitude'=>$req->latitude,
                    'longitude'=>$req->longitude
                ]);
                if($shop)
                {
                    return response()->json(['token'=> $token,'message'=> 'successfully Register'],200);
                }
            }
        } //user
        else
        {
            return response()->json(['message'=>'Cannot Create User'],403);
        }
        
    }
    public function fetchCountry()
    {
      //  $user = Auth::user();
        $countries = Country::pluck('country_name');
        return response()->json(['countries'=>$countries]);
        
    }
    public function checkEmail(Request $req)
    {
      //  $user = Auth::user();
        $user = User::where('email', '=', $req->email)->first();
        if($user)
            return response()->json(['status'=>'Email already Exist'],403);
        
        
    }
    /**
     * Verify the user if it exist or not.
     *
     * @return \Illuminate\Http\Response
     */
    public function verify(Request $req)
    {
        //
        return $req->user()->only('name');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
