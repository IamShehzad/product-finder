<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            "name" => $this->name,
            "description" => substr($this->description,0,150).'...',
            "price" => $this->price,
            "thumbnail" => $this->thumbnail,
            "category_id" => $this->category->name,
            "brand_id" => $this->brand->name

        ];
    }
}
