<?php

namespace App\Models;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Shop;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function category()
    {
    	return $this->belongsTo(Category::class);
    }
    public function brand()
    {
    	return $this->belongsTo(Brand::class);
    }
    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }
}
