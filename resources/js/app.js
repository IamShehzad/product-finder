/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;
import vuetify from './vuetify.js'
import router from './router.js'
import App from './components/App'


// Vue.component('apexchart', VueApexCharts)
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import * as VueGoogleMaps from 'vue2-google-maps'
import VueGeolocation from 'vue-browser-geolocation';
Vue.use(VueGeolocation);
Vue.use(VueGoogleMaps, {
  load: {
    // key: 'AIzaSyDoTJeWOIql74invfx88d6f6gMIc67JHL4',
    key: 'AIzaSyDp4VYtAuD9F3oaNzisX5xfPTLYsRn3RUg',
    libraries: ['places', ],
    mapId: 'dfa96054600eed6c',
    mapTypeControl: false,
  }});

new Vue({
    el: '#app',
    vuetify,
    router,
    components:{
    	App
    }
});
