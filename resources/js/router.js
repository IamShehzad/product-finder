import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import Admin from './components/Admin'
import Result from './components/Result.vue';
// import Testing from './components/Testing.vue';
import Main from './components/Main'
import Dashboard from './components/admin/Dashboard'
import Brand from './components/admin/Brand'
import Categories from './components/admin/Categories'
import Products from './components/admin/Products'
import Ads from './components/admin/Ads'
import Package from './components/admin/Package'
import Account from './components/admin/Account'
import Shop from './components/admin/Shop'
import Login from './components/Login'
import Registration from './components/Registration'
import ViewDetails from './components/View.vue'
const routes = [
	{
		name:'Main',
		path:'/',
		component:Main,
	},
	{
		name:'Result',
		path:'/result',
		component:Result,
		props:true,
		
	},
	{
		name:'ViewProduct',
		path:'/view/:id/:name/:distance/:shop',
		component:ViewDetails,
		props:true,
		
	},

	{
		name:'Registration',
		path:'/register',
		component:Registration,
		props:true
	},
	{
		name:'Login',
		path:'/login',
		component:Login,
		beforeEnter(to,from,next)
		{
			let logged = localStorage.getItem('token');
			if(!logged)
				next();
			else
				next('/admin/dashboard');

		}

	},
	{
		name:'Admin',
		path:'/admin',
		redirect: 'admin/dashboard',
		component:Admin,
		children:[
			{path:'dashboard',component:Dashboard},
			{path:'products',component:Products},
			{path:'advertisment',component:Ads},
			{path:'package',component:Package},
			{path:'account',component:Account},
			{path:'shop',component:Shop},
			{path:'brand',component:Brand},
			{path:'categories',component:Categories}

		],
		beforeEnter(to,from,next)
		{
			axios.get('/api/verify')
			.then(res=> next())
			.catch(err=> {next('/login')});
		}
	}
];

const router = new VueRouter({routes});
//hr router ko access krny sa pahly
router.beforeEach((to,from,next)=>{
	//token agr hy to wo otherwise null
	if(to.name=='Main')
		next()
	else
		{const token= localStorage.getItem('token') || null;
		window.axios.defaults.headers['Authorization'] = 'Bearer '+token;
		next();}
	//hm na attach kr deya token for every axios request
	//laravel will automatic authenticate the user if it is
	//authorized then it will return record other wise it did
	//not send the record
	//laravel will match the token_id value with database 
	
});
export default router;

// '../view/'+product.id+'/'+product.name