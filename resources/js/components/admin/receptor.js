//receptor start
axios.interceptors.request.use(e=>(this.loading=!0,e),e=>(this.loading=!1,Promise.reject(e))),axios.interceptors.response.use(e=>(this.loading=!1,e),e=>(this.loading=!1,this.text="Unauthorized",this.snackbar=!0,Promise.reject(e)));
//receptor end