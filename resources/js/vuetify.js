import Vue from 'vue'
import Vuetify from 'vuetify'

import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify,{
  iconfont: 'md',
  theme: {
    themes: {
        dark: {
          primary: '#3F51B5',
          accent: '#5603DF',
          secondary: '#17171F',
          success: '#4CAF50',
          info: '#F8F8F9',
          warning: '#FB8C00',
          error: '#FF5252',
          

        },
        light: {
          primary: '#0127FD',
          accent1: '#0027FF',
          secondary: '#17171F',
          success: '#4CAF50',
          info: '#2196F3',
          warning: '#FB8C00',
          error: '#FF5252',
          anchor:'#ff004e',
          card1_front:'#da0346'
        }
    }
  },
});
export default new Vuetify({})